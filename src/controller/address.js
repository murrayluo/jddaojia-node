/**
 @author huzibaile
 @date Created in 15:18 2022/09/14
 @description 收货地址相关的数据操作
 */

const Address = require('../models/Address')

/**
 * 新增收货地址
 * @param addressInfo {Object}
 * @returns
 */
const createAddress = async (addressInfo) => {
  const address = await Address.create(addressInfo)
  return address
}

/**
 * 获取地址列表
 * @param {string} username 用户名
 * @returns
 */
async function getAddressList(username) {
  const list = await Address.find({ username }).sort({ updatedAt: -1 }) // 按更新时间倒序
  return list
}

/**
 * 查找单个收货地址
 * @param {string} id id
 * @returns
 */
async function getAddressById(id) {
  const address = Address.findById(id)
  return address
}

/**
 * 更新地址信息
 * @param id {string}
 * @param data {Object}
 * @returns
 */
async function updateAddress(id, data) {
  const address = await Address.findOneAndUpdate({ _id: id }, data, { new: true }) // 返回更新之后的最新数据，默认是 false 返回更新之前的数据
  return address
}

module.exports = { createAddress, getAddressList, getAddressById, updateAddress }
