/**
 @author huzibaile
 @date Created in 17:50 2022/09/13
 @description 用户集合相关的数据操作
 */

const User = require('../models/User')

/**
 * 注册用户
 * @param userInfo {username,password}
 * @returns {Promise<HydratedDocument<unknown, {}, {}>[]>}
 */
const register = async (userInfo) => {
  const user = await User.create(userInfo)
  return user
}

/**
 * 登录
 * @param userInfo {username,password}
 * @returns
 */
const login = async (userInfo) => {
  const user = await User.findOne(userInfo)
  return !!user
}

module.exports = { register, login }
