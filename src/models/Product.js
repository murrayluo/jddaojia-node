/**
 @author huzibaile
 @date Created in 17:37 2022/09/11
 @description 商品信息
 */

const { mongoose } = require('../db/mongo')

const Schema = new mongoose.Schema({
  shopId: {
    type: String,
    require: true
  },
  name: String,
  imgUrl: String,
  sales: Number,
  price: Number,
  oldPrice: Number,
  tabs: [ String ] //示例 tabs:['all','seckill']
}, { timestamps: true })

const Product = mongoose.model('product', Schema)

module.exports = Product
