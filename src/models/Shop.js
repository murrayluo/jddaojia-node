/**
 @author huzibaile
 @date Created in 17:24 2022/09/11
 @description
 */

const { mongoose } = require('../db/mongo')

const Schema = new mongoose.Schema({
  name: String,
  imgUrl: String,
  sales: String,
  expressLimt: {
    type: Number,
    default: 0
  },
  expressPrice: Number,
  slogan: String
}, { timestamps: true })

const Shop = mongoose.model('shop', Schema)

module.exports = Shop
