/**
 @author huzibaile
 @date Created in 17:15 2022/09/11
 @description 收货地址信息
 */

const { mongoose } = require('../db/mongo')

const AddressSchema = new mongoose.Schema({
  username: {
    type: String,
    require: true
  },
  city: String,
  department: String,
  houseNumber: String,
  name: String,
  phone: String
}, { timestamps: true })

const Address = mongoose.model('address', AddressSchema)

module.exports = Address
