/**
 @author huzibaile
 @date Created in 16:48 2022/09/11
 @description 用户信息
 */

const { mongoose } = require('../db/mongo')

const UserSchema = mongoose.Schema({
  username: {
    type: String,
    require: true,
    unique: true
  },
  password: {
    type: String,
    require: true
  }
}, { timestamps: true })

const User = mongoose.model('user', UserSchema)

module.exports = User
