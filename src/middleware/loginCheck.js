/**
 @author huzibaile
 @date Created in 15:05 2022/09/11
 @description 登录验证相关的逻辑
 */

const { ErrorModel } = require('../utils/resModel')
module.exports = async (ctx, next) => {
  const session = ctx.session
  if (session && session.userInfo) {
    await next()
  } else {
    ctx.body = new ErrorModel(10003, '用户未登录')
  }
  
}
