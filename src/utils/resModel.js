/**
 @author huzibaile
 @date Created in 19:18 2022/09/13
 @description 响应返回数据的规范
 */

class SuccessModel {
  constructor(data, message = '成功') {
    this.erron = 0
    this.message = message
    if (data) this.data = data
  }
}


class ErrorModel {
  constructor(erron, message = '失败') {
    this.erron = erron
    this.message = message
  }
}


module.exports = { SuccessModel, ErrorModel }
