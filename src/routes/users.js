const router = require('koa-router')()

const { register, login } = require('../controller/user')
const { SuccessModel, ErrorModel } = require('../utils/resModel')


router.prefix('/api/user')

//注册用户
router.post('/register', async (ctx, next) => {
  try {
    const user = await register(ctx.request.body)
    ctx.body = new SuccessModel(user)
  } catch (e) {
    console.error(e)
    ctx.body = new ErrorModel(10001, `注册失败:${ e.message }`)
  }
})

// 登录
router.post('/login', async (ctx, next) => {
  const userInfo = ctx.request.body
  const tag = await login(userInfo)
  if (tag) {
    ctx.session.userInfo = { username: userInfo.username }
    ctx.body = new SuccessModel()
  } else {
    ctx.body = new ErrorModel(10002, '登录失败')
  }
})

module.exports = router
