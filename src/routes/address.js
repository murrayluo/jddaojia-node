/**createAddress
 @author huzibaile
 @date Created in 21:06 2022/09/13
 @description 收货地址相关路由
 */
const router = require('koa-router')()
const { createAddress, getAddressList, getAddressById, updateAddress } = require('../controller/address')
const { SuccessModel, ErrorModel } = require('../utils/resModel')
const loginCheck = require('../middleware/loginCheck')

router.prefix('/api/user/address')
// 创建收货地址
router.post('/', loginCheck, async (ctx, next) => {
  console.log(ctx.session.userInfo)
  const username = ctx.session.userInfo.username
  const addressInfo = ctx.request.body
  addressInfo.username = username
  try {
    const addressObj = await createAddress(addressInfo)
    ctx.body = new SuccessModel(addressObj)
  } catch (e) {
    console.error(e)
    ctx.body = new ErrorModel(10004, `创建失败-${ e.message }`)
  }
})

// 获取收货地址列表
router.get('/', loginCheck, async function (ctx, next) {
  // 有登录验证，可以直接获取 session
  const userInfo = ctx.session.userInfo
  const username = userInfo.username
  // 获取列表
  const list = await getAddressList(username)
  ctx.body = new SuccessModel(list)
})

// 获取单个收货地址
router.get('/:id', loginCheck, async function (ctx, next) {
  const id = ctx.params.id // 获取路由的动态参数
  const address = await getAddressById(id)
  ctx.body = new SuccessModel(address)
})

// 更新收货地址
router.patch('/:id', loginCheck, async function (ctx, next) {
  const id = ctx.params.id // 获取路由的动态参数
  const data = ctx.request.body || {} // 前端传来的数据
  data.username = ctx.session.userInfo.username
  
  // 更新
  const newAddress = await updateAddress(id, data)
  ctx.body = new SuccessModel(newAddress)
})
module.exports = router
