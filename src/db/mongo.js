/**
 @author huzibaile
 @date Created in 19:44 2022/09/10
 @description 连接数据mongodb
 */

const mongoose = require('mongoose')

const url = 'mongodb://127.0.0.1:27017'
const dbName = 'jd'

// 创建连接
mongoose.connect(`${ url }/${ dbName }`)
const db = mongoose.connection

db.on('error', err => {
  console.error('mongoose connect error', err)
})

module.exports = { mongoose, db }
