/**
 @author huzibaile
 @date Created in 17:56 2022/09/11
 @description
 */
const User = require('../../models/User')

!(async () => {
  // 注册:创建一个新的用户
  await User.create({
    username: 'xiaomo01',
    password: '123'
  })
  // 再建一个新的用户
  await User.create({
    username: '13120408213',
    password: '123'
  })
  
  // 登录：查询单个用户
  const zhangsan = await User.findOne({
    username: 'xiaomo01',
    password: '123'
  })
  console.log('zhangsan', zhangsan)
})()
